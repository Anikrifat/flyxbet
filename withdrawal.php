<?php include('inc/header.php') ?>

<div class="container">
	<div class="row">

		<div class="site-content col-md-12">
			<div class="row justify-content-center" style="display:none;">
				<div class="col-md-12">
					<h1 class="page-title">My Account</h1>
				</div>
			</div>
			<div class="row">

				<div class="col-md-12">
					<h2 class="page-title">Withdrawal</h2>
					<div class="row justify-content-center mb-4">
						<div class="col-12 col-sm-12 col-md-12 col-lg-12 col-xl-12">
							<div class="transfer-tabs mb-1">
								<ul class="nav nav-tabs">
									<li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#withdrawalList"><span class="link-label">Withdrawal</span></a></li>
									<li class="nav-item"><a class="nav-link" data-toggle="tab" href="#withdrawalHistory"><span class="link-label">Withdrawal History</span></a></li>
								</ul>
							</div>
							<div id="transferTabContent" class="tab-content">
								<div class="tab-pane fade active show" id="withdrawalList">
									<div class="card card-balance-withdraw bg-white">
										<div class="card-body">
											<ul class="withdraw-methods">
												<li class="item">
													<a href="javascript:void(0)" data-method="nagad" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-nagad" title="Nagad"><img src="assets/img/pay/method-nagad.png" alt="Nagad"><span class="method-title">Nagad</span></a>
													<div class="modal modal-withdraw modal-nagad fade" id="withdrawModal-nagad" tabindex="-1" role="dialog" aria-labelledby="withdrawModalnagadLabel" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="withdrawModalnagadLabel">
																		<img src="https://www.bet3up.com/uploads/methods/method-nagad.png" alt="Nagad">
																		<span class="user-balance">$0 USD</span>
																	</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">×</span>
																	</button>
																</div>
																<div class="modal-body">
																	<form id="frmBalanceWithdrawNagad" class="form-balance-withdraw" method="POST" action="https://www.bet3up.com/withdraw-confirm-nagad" novalidate="novalidate">
																		<input type="hidden" name="_token" value="t6iPsPT6gYKeCER2sg6VegwOgnZYPzH8ldmoFlvS">
																		<div class="form-group row">
																			<label for="nagadWithdrawAmount" class="col-4 col-form-label">Amount</label>
																			<div class="col-8">
																				<input id="nagadWithdrawAmount" type="number" inputmode="decimal" class="form-control " name="nagad_amount" value="" placeholder="0.00 USD" required="" autocomplete="off" autofocus="">
																			</div>
																		</div>
																		<div class="form-group row">
																			<label for="nagadWithdrawNumber" class="col-4 col-form-label" style="font-weight:500;">Your Nagad (নগদ পারসোনাল) wallet number:</label>
																			<div class="col-8">
																				<input id="nagadWithdrawNumber" type="number" inputmode="decimal" class="form-control " name="nagad_wallet_number" value="" placeholder="01xxxxxxxxx">
																				<small id="emailHelp" class="form-text text-muted">এজেন্ট নম্বর এ টাকা প্রধান করা হয় না</small>
																			</div>
																		</div>

																		<div class="form-group row">
																			<label for="nagadReceiverCurrency" class="col-4 col-form-label">Receiver Currency:</label>
																			<div class="col-8"><span class="form-control" style="text-align:left;line-height:1.8;">BDT (বাংলাদেশী টাকা)</span></div>
																		</div>
																		<div class="form-group row mb-0">
																			<div class="col-md-6 offset-md-3">
																				<button id="btnWithdrawConfirmNagad" type="submit" class="btn btn-withdraw-confirm btn-success btn-block">
																					Withdrawal Confirm
																				</button>
																			</div>
																		</div>
																	</form>

																</div>
															</div>
														</div>
													</div>
												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="bkash" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-bkash" title="bKash"><img src="assets/img/pay/method-bkash.png" alt="bKash"><span class="method-title">bKash</span></a>
													<div class="modal modal-withdraw modal-bkash fade" id="withdrawModal-bkash" tabindex="-1" role="dialog" aria-labelledby="withdrawModalbkashLabel" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="withdrawModalbkashLabel">
																		<img src="https://www.bet3up.com/uploads/methods/method-bkash.png" alt="bKash">
																		<span class="user-balance">$0 USD</span>
																	</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">×</span>
																	</button>
																</div>
																<div class="modal-body">
																	<form id="frmBalanceWithdrawBkash" class="form-balance-withdraw" method="POST" action="https://www.bet3up.com/withdraw-confirm-bkash" novalidate="novalidate">
																		<input type="hidden" name="_token" value="t6iPsPT6gYKeCER2sg6VegwOgnZYPzH8ldmoFlvS">
																		<div class="form-group row">
																			<label for="bkashWithdrawAmount" class="col-4 col-form-label">Amount</label>
																			<div class="col-8">
																				<input id="bkashWithdrawAmount" type="number" inputmode="decimal" class="form-control " name="bkash_amount" value="" placeholder="0.00 USD" required="" autocomplete="off" autofocus="">
																			</div>
																		</div>
																		<div class="form-group row">
																			<label for="bkashWithdrawNumber" class="col-4 col-form-label" style="font-weight:500;">Your bKash (বিকাশ পারসোনাল) wallet number:</label>
																			<div class="col-8">
																				<input id="bkashWithdrawNumber" type="number" inputmode="decimal" class="form-control " name="bkash_wallet_number" value="" placeholder="01xxxxxxxxx">
																				<small id="bkashEmailHelp" class="form-text text-muted">এজেন্ট নম্বর এ টাকা প্রধান করা হয় না</small>
																			</div>
																		</div>

																		<div class="form-group row">
																			<label for="bkashReceiverCurrency" class="col-4 col-form-label">Receiver Currency:</label>
																			<div class="col-8"><span class="form-control" style="text-align:left;line-height:1.8;">BDT (বাংলাদেশী টাকা)</span></div>
																		</div>
																		<div class="form-group row mb-0">
																			<div class="col-md-6 offset-md-3">
																				<button id="btnWithdrawConfirmbKash" type="submit" class="btn btn-withdraw-confirm btn-success btn-block">
																					Withdrawal Confirm
																				</button>
																			</div>
																		</div>
																	</form>

																</div>
															</div>
														</div>
													</div>
												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="rocket" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-rocket" title="Rocket"><img src="assets/img/pay/method-rocket.png" alt="Rocket"><span class="method-title">Rocket</span></a>
													<div class="modal modal-withdraw modal-rocket fade" id="withdrawModal-rocket" tabindex="-1" role="dialog" aria-labelledby="withdrawModalrocketLabel" aria-hidden="true">
														<div class="modal-dialog modal-dialog-centered" role="document">
															<div class="modal-content">
																<div class="modal-header">
																	<h5 class="modal-title" id="withdrawModalrocketLabel">
																		<img src="https://www.bet3up.com/uploads/methods/method-rocket.png" alt="Rocket">
																		<span class="user-balance">$0 USD</span>
																	</h5>
																	<button type="button" class="close" data-dismiss="modal" aria-label="Close">
																		<span aria-hidden="true">×</span>
																	</button>
																</div>
																<div class="modal-body">
																	<form id="frmBalanceWithdrawRocket" class="form-balance-withdraw" method="POST" action="https://www.bet3up.com/withdraw-confirm-rocket" novalidate="novalidate">
																		<input type="hidden" name="_token" value="t6iPsPT6gYKeCER2sg6VegwOgnZYPzH8ldmoFlvS">
																		<div class="form-group row">
																			<label for="rocketWithdrawAmount" class="col-4 col-form-label">Amount</label>
																			<div class="col-8">
																				<input id="rocketWithdrawAmount" type="number" inputmode="decimal" class="form-control " name="rocket_amount" value="" placeholder="0.00 USD" required="" autocomplete="off" autofocus="">
																			</div>
																		</div>
																		<div class="form-group row">
																			<label for="rocketWithdrawNumber" class="col-4 col-form-label" style="font-weight:500;">Your Rocket (রকেট পারসোনাল) wallet number:</label>
																			<div class="col-8">
																				<input id="rocketWithdrawNumber" type="number" inputmode="decimal" class="form-control " name="rocket_wallet_number" value="" placeholder="01xxxxxxxxx">
																				<small id="rocketEmailHelp" class="form-text text-muted">এজেন্ট নম্বর এ টাকা প্রধান করা হয় না</small>
																			</div>
																		</div>

																		<div class="form-group row">
																			<label for="rocketReceiverCurrency" class="col-4 col-form-label">Receiver Currency:</label>
																			<div class="col-8"><span class="form-control" style="text-align:left;line-height:1.8;">BDT (বাংলাদেশী টাকা)</span></div>
																		</div>
																		<div class="form-group row mb-0">
																			<div class="col-md-6 offset-md-3">
																				<button id="btnWithdrawConfirmRocket" type="submit" class="btn btn-withdraw-confirm btn-success btn-block">
																					Withdrawal Confirm
																				</button>
																			</div>
																		</div>
																	</form>

																</div>
															</div>
														</div>
													</div>
												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="bank" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-bank" title="Bank"><img src="assets/img/pay/method-bank.png" alt="Bank"><span class="method-title">Bank</span></a>

												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="westernunion" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-westernunion" title="Western Union"><img src="assets/img/pay/method-westernunion.png" alt="Western Union"><span class="method-title">Western</span></a>

												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="imetransfer" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-imetransfer" title="IME Transfer"><img src="assets/img/pay/method-imetransfer.png" alt="IME Transfer"><span class="method-title">IME</span></a>

												</li>

												<li class="item">
													<a href="javascript:void(0)" data-method="paytm" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-paytm" title="Paytm"><img src="assets/img/pay/method-paytm.png" alt="Paytm"><span class="method-title">Paytm</span></a>

												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="phonepe" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-phonepe" title="PhonePe"><img src="assets/img/pay/method-phonepe.png" alt="PhonePe"><span class="method-title">PhonePe</span></a>

												</li>
												<li class="item">
													<a href="javascript:void(0)" data-method="jazzcash" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-jazzcash" title="JazzCash"><img src="assets/img/pay/method-jazzcash.png" alt="JazzCash"><span class="method-title">JazzCash</span></a>

												</li>
											</ul>
										</div>
									</div>
								</div>
								<div class="tab-pane fade" id="withdrawalHistory">
									<div class="card card-withdraw-history" style="max-width: 100%;">
										<div class="card-header filter-header">
											<label class="filter-label">Sort by:</label>
											<div class="filter-field">
												<select id="withdrawSortBy" class="form-control" name="withdraw_method">
													<option value="*">All</option>
													<option value=".bank">Bank Withdraw</option>
													<option value=".westernunion">Western Union</option>
													<option value=".imetransfer">IME Transfer</option>
													<option value=".nagad">Nagad Withdraw</option>
													<option value=".bkash">bKash Withdraw</option>
													<option value=".rocket">Rocket Withdraw</option>
													<option value=".paytm">Paytm</option>
													<option value=".googlepay">Google Pay</option>
													<option value=".phonepe">PhonePe</option>
													<option value=".jazzcash">JazzCash</option>
												</select>
											</div>
										</div>
										<div class="card-body">
											<p class="card-text">There is not enough withdraw record to listed.</p>
										</div>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<?php include('inc/footer.php') ?>