<?php include('inc/header.php') ?>

<div class="container">
	<div class="card card-balance-withdraw bg-white">
		<div class="card-body">
			<ul class="withdraw-methods">
				<li class="item">
					<a href="javascript:void(0)" data-method="nagad" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-nagad" title="Nagad"><img src="assets/img/pay/method-nagad.png" alt="Nagad"><span class="method-title">Nagad</span></a>
					<div class="modal modal-withdraw modal-nagad fade" id="withdrawModal-nagad" tabindex="-1" role="dialog" aria-labelledby="withdrawModalnagadLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
							<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="withdrawModalnagadLabel">
										<img src="https://www.bet3up.com/uploads/methods/method-nagad.png" alt="Nagad">
										<span class="user-balance">৳0 টাকা</span>
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">×</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmBalanceWithdrawNagad" class="form-balance-withdraw" method="POST" action="#" novalidate="novalidate">
										<div class="form-group row">
											<label for="nagadWithdrawAmount" class="col-4 col-form-label">Amount (সর্বনিম্ন ২০০ টাকা)</label>
											<div class="col-8">
												<input id="nagadWithdrawAmount" type="number" inputmode="decimal" class="form-control " name="nagad_amount" value="" placeholder="0.00 USD" required="" autocomplete="off" autofocus="">
											</div>
										</div>
										<div class="form-group row">
											<label for="nagadWithdrawNumber" class="col-4 col-form-label" style="font-weight:500;">Your Nagad wallet number:</label>
											<div class="col-8">
												<input id="nagadWithdrawNumber" type="number" inputmode="decimal" class="form-control " name="nagad_wallet_number" value="" placeholder="01xxxxxxxxx">

											</div>
										</div>
										<div class="form-group row">
											<label for="nagadtrans" class="col-4 col-form-label">Transaction Id:</label>
											<div class="col-8"><input class="form-control" placeholder="Transaction id" style="text-align:left;line-height:1.8;"></div>
										</div>
										<div class="form-group container">
											<label for="nagadtrans" class="col-12 text-left col-form-label">Rocket Number: 01643675060</label>
										</div>
										<div class="form-group row mb-0">
											<div class="col-md-6 offset-md-3">
												<button id="btnWithdrawConfirmNagad" type="submit" class="btn btn-withdraw-confirm btn-success btn-block">
													Deposit Confirm
												</button>
											</div>
										</div>
										
									</form>

								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="bkash" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-bkash" title="bKash"><img src="assets/img/pay/method-bkash.png" alt="bKash"><span class="method-title">bKash</span></a>
					<div class="modal modal-withdraw modal-bkash fade" id="withdrawModal-bkash" tabindex="-1" role="dialog" aria-labelledby="withdrawModalbkashLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="withdrawModalnagadLabel">
										<img src="https://www.bet3up.com/uploads/methods/method-bkash.png" alt="Nagad">
										<span class="user-balance">৳0 টাকা</span>
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">×</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmBalanceWithdrawNagad" class="form-balance-withdraw" method="POST" action="#" novalidate="novalidate">
										<div class="form-group row">
											<label for="nagadWithdrawAmount" class="col-4 col-form-label">Amount (সর্বনিম্ন ২০০ টাকা)</label>
											<div class="col-8">
												<input id="nagadWithdrawAmount" type="number" inputmode="decimal" class="form-control " name="nagad_amount" value="" placeholder="0.00 USD" required="" autocomplete="off" autofocus="">
											</div>
										</div>
										<div class="form-group row">
											<label for="nagadWithdrawNumber" class="col-4 col-form-label" style="font-weight:500;">Your Nagad wallet number:</label>
											<div class="col-8">
												<input id="nagadWithdrawNumber" type="number" inputmode="decimal" class="form-control " name="nagad_wallet_number" value="" placeholder="01xxxxxxxxx">

											</div>
										</div>
										<div class="form-group row">
											<label for="nagadtrans" class="col-4 col-form-label">Transaction Id:</label>
											<div class="col-8"><input class="form-control" placeholder="Transaction id" style="text-align:left;line-height:1.8;"></div>
										</div>
										<div class="form-group container">
											<label for="nagadtrans" class="col-12 text-left col-form-label">Rocket Number: 01643675060</label>
										</div>
										<div class="form-group row mb-0">
											<div class="col-md-6 offset-md-3">
												<button id="btnWithdrawConfirmNagad" type="submit" class="btn btn-withdraw-confirm btn-success btn-block">
													Deposit Confirm
												</button>
											</div>
										</div>
										
									</form>

								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="rocket" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-rocket" title="Rocket"><img src="assets/img/pay/method-rocket.png" alt="Rocket"><span class="method-title">Rocket</span></a>
					<div class="modal modal-withdraw modal-rocket fade" id="withdrawModal-rocket" tabindex="-1" role="dialog" aria-labelledby="withdrawModalrocketLabel" aria-hidden="true">
						<div class="modal-dialog modal-dialog-centered" role="document">
						<div class="modal-content">
								<div class="modal-header">
									<h5 class="modal-title" id="withdrawModalnagadLabel">
										<img src="https://www.bet3up.com/uploads/methods/method-rocket.png" alt="Nagad">
										<span class="user-balance">৳0 টাকা</span>
									</h5>
									<button type="button" class="close" data-dismiss="modal" aria-label="Close">
										<span aria-hidden="true">×</span>
									</button>
								</div>
								<div class="modal-body">
									<form id="frmBalanceWithdrawNagad" class="form-balance-withdraw" method="POST" action="#" novalidate="novalidate">
										<div class="form-group row">
											<label for="nagadWithdrawAmount" class="col-4 col-form-label">Amount (সর্বনিম্ন ২০০ টাকা)</label>
											<div class="col-8">
												<input id="nagadWithdrawAmount" type="number" inputmode="decimal" class="form-control " name="nagad_amount" value="" placeholder="0.00 USD" required="" autocomplete="off" autofocus="">
											</div>
										</div>
										<div class="form-group row">
											<label for="nagadWithdrawNumber" class="col-4 col-form-label" style="font-weight:500;">Your Nagad wallet number:</label>
											<div class="col-8">
												<input id="nagadWithdrawNumber" type="number" inputmode="decimal" class="form-control " name="nagad_wallet_number" value="" placeholder="01xxxxxxxxx">

											</div>
										</div>
										<div class="form-group row">
											<label for="nagadtrans" class="col-4 col-form-label">Transaction Id:</label>
											<div class="col-8"><input class="form-control" placeholder="Transaction id" style="text-align:left;line-height:1.8;"></div>
										</div>
										<div class="form-group container">
											<label for="nagadtrans" class="col-12 text-left col-form-label">Rocket Number: 01643675060</label>
										</div>
										<div class="form-group row mb-0">
											<div class="col-md-6 offset-md-3">
												<button id="btnWithdrawConfirmNagad" type="submit" class="btn btn-withdraw-confirm btn-success btn-block">
													Deposit Confirm
												</button>
											</div>
										</div>
										
									</form>

								</div>
							</div>
						</div>
					</div>
				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="bank" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-bank" title="Bank"><img src="assets/img/pay/method-bank.png" alt="Bank"><span class="method-title">Bank</span></a>

				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="westernunion" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-westernunion" title="Western Union"><img src="assets/img/pay/method-westernunion.png" alt="Western Union"><span class="method-title">Western</span></a>

				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="imetransfer" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-imetransfer" title="IME Transfer"><img src="assets/img/pay/method-imetransfer.png" alt="IME Transfer"><span class="method-title">IME</span></a>

				</li>

				<li class="item">
					<a href="javascript:void(0)" data-method="paytm" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-paytm" title="Paytm"><img src="assets/img/pay/method-paytm.png" alt="Paytm"><span class="method-title">Paytm</span></a>

				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="phonepe" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-phonepe" title="PhonePe"><img src="assets/img/pay/method-phonepe.png" alt="PhonePe"><span class="method-title">PhonePe</span></a>

				</li>
				<li class="item">
					<a href="javascript:void(0)" data-method="jazzcash" class="withdraw-method" data-toggle="modal" data-target="#withdrawModal-jazzcash" title="JazzCash"><img src="assets/img/pay/method-jazzcash.png" alt="JazzCash"><span class="method-title">JazzCash</span></a>

				</li>
			</ul>
		</div>
	</div>
</div>
<?php include('inc/footer.php') ?>