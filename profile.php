<?php include('inc/header.php') ?>

<div class="container">
    <div class="row">

        <div class="site-content col-12 ">
            <div class="row justify-content-center">
                <div class="col-md-12">
                    <h1 class="page-title">My Profile</h1>
                </div>
            </div>
            <div class="card card-custom bg-white mb-3">
                <div class="card-body">
                    <div class="profile-header">
                        <div class="user-info">
                            <div class="user-photo">
                                <span class="material-icons">account_circle</span>
                                <span class="user-country"><i class="flag-icon flag-icon-bd"></i></span>
                            </div>
                            <div class="user-details">
                                <span class="user-name">
                                    anik
                                    rifat
                                </span>
                                <span class="user-id">User ID: 14336245 </span>
                                <span class="user-country-name label-with-icons"><span class="material-icons">place</span> <span class="label-text">Bangladesh</span></span>
                            </div>
                        </div>
                        <div class="user-status">
                            <span class="user-match label-with-icons">
                                <span class="material-icons text-success">verified</span> <span class="label-text text-success">Verified</span>
                            </span>
                        </div>
                    </div>
                    <div class="profile-actions">
                        <a href="edit-profile.php" class="btn btn-info text-center label-with-icons">
                            <span class="material-icons">settings</span>
                            <span class="label-text">Edit Profile</span>
                        </a>
                        <a href="https://www.bet3up#/change-profile-photo" class="btn btn-info text-center label-with-icons">
                            <span class="material-icons">camera_alt</span>
                            <span class="label-text">Change Picture</span>
                        </a>
                        <a href="https://www.bet3up#/change-password" class="btn btn-info text-center label-with-icons">
                            <span class="material-icons">vpn_key</span>
                            <span class="label-text">Change Password</span>
                        </a>
                    </div>
                    <div class="profile-informations">
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">directions_walk</span> <span class="label-text">Male</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">person</span> <span class="label-text">
                                    anik
                                    rifat
                                </span></span>
                        </div>
                        <div class="profile-item verified">
                            <span class="user-name label-with-icons"><span class="material-icons">smartphone</span> <span class="label-text">+8801643675060</span></span>
                            <span class="user-verified"><span class="material-icons text-success">verified_user</span></span>
                        </div>
                        <div class="profile-item verified">
                            <span class="user-name label-with-icons"><span class="material-icons">mail</span> <span class="label-text">reafatul@gmail#</span></span>
                            <span class="user-verified"><span class="material-icons text-success">verified_user</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">vpn_key</span> <span class="label-text"></span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">perm_contact_calendar</span> <span class="label-text">2001-10-02</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">leaderboard</span> <span class="label-text">LEVEL: 0</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">assignment_ind</span> <span class="label-text">Sponsor: </span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">person_add</span> <span class="label-text">Register by Email</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">insert_link</span> <span class="label-text">https://www.bet3up#/register?ref=14336245</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">date_range</span> <span class="label-text">2021-07-12 21:06:06</span></span>
                        </div>
                        <div class="profile-item">
                            <span class="user-name label-with-icons"><span class="material-icons">insert_link</span> <span class="label-text">Active</span></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php include('inc/footer.php') ?>