<?php include('inc/header.php') ?>
<section id="sectionBannerSlider" class="page-section section-banner-slider">
	<div id="blockBannerSlider" class="page-block block-banner-slider bg-primary py-0 px-0">
		<div class="home-banner-slider">
			<div class="slide" style="background-image:url('assets/img/banners/banner-05.jpg')">
				<img class="slide-image" src="assets/img/banners/banner-05.jpg" alt="5" />
				<div class="container">
					<div class="caption">
						<h2 class="animate__animated animate__backInDown">5</h2>
						<div class="animate__animated animate__fadeInUp"></div>
					</div>
				</div>
			</div>
			<div class="slide" style="background-image:url('assets/img/banners/banner-04.jpg')">
				<img class="slide-image" src="assets/img/banners/banner-04.jpg" alt="4" />
				<div class="container">
					<div class="caption">
						<h2 class="animate__animated animate__backInDown">4</h2>
						<div class="animate__animated animate__fadeInUp"></div>
					</div>
				</div>
			</div>
			<div class="slide" style="background-image:url('assets/img/banners/banner-03.jpg')">
				<img class="slide-image" src="assets/img/banners/banner-03.jpg" alt="Banner Football" />
				<div class="container">
					<div class="caption">
						<h2 class="animate__animated animate__backInDown">Banner Football</h2>
						<div class="animate__animated animate__fadeInUp">
							<p>Lorem Ipsum is simply dummy text of the printing and typesetting industry.
							</p>
						</div>
						<a class="btn btn-outline-primary animate__fadeInUp animate__animated" href="https://www.google.com/">Get Started</a>
					</div>
				</div>
			</div>
		</div>

	</div>
</section>
<section class="all-games-section">
	<div class="game-section bg-indigo">
		<div class="container py-2">
			<div class="game-head d-flex justify-content-between align-items-center">
				<div class="sec-title">
					<h5 class="text-white">
						Live-Casino <small class="text-primary-alt">5774</small>
					</h5>
				</div>
				<div class="sec-details text-primary-alt">
					All <span><i class="fas fa-angle-double-right"></i></span>
				</div>
			</div>
			<div class="game-sec">
				<div class="row">
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					



				</div>
			</div>
		</div>
	</div>
	<div class="game-section bg-indigo">
		<div class="container py-2">
			<div class="game-head d-flex justify-content-between align-items-center">
				<div class="sec-title">
					<h5 class="text-white">
						Live-Casino <small class="text-primary-alt">5774</small>
					</h5>
				</div>
				<div class="sec-details text-primary-alt">
					All <span><i class="fas fa-angle-double-right"></i></span>
				</div>
			</div>
			<div class="game-sec">
				<div class="row">
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					<div class="col-lg-2 col-md-3 col-4">
						<div class="games-box-img my-2">
							<a href="#">
								<img src="assets/img/g1.gif" alt="" class="game-img img-fluid w-100 h-100">
							</a>
						</div>
					</div>
					



				</div>
			</div>
		</div>
	</div>
</section>
<?php include('inc/footer.php') ?>