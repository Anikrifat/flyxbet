
		<?php include('inc/header.php') ?>
			<div class="container">
				<div class="row justify-content-center">
					<div class="col-md-8">
						<div class="card-group">
							<div class="card card-login p-4">
								<div class="card-body">
									<h1>Login</h1>
									<p class="text-muted">Sign In to your account</p>
									<form id="frmUserLogin" method="POST" action="#login">
										<div class="input-group mb-3">
											<input id="username" type="text" class="form-control username " name="username" value="" placeholder="ID, email, phone" required autocomplete="username" autofocus>
										</div>
										<div class="input-group mb-3">
											<input id="password" type="password" class="form-control password " name="password" placeholder="Password" required autocomplete="current-password">
											<span class="password-show-hide" title="Show password"><span class="material-icons">visibility_off</span></span>
										</div>
										<div class="form-group row mb-2">
											<div class="col-md-12">
												<div class="form-check">
													<input class="form-check-input" type="checkbox" name="remember" id="remember">
													<label class="form-check-label" for="remember">
														Remember Me
													</label>
												</div>
												<a class="btn btn-link float-right" href="password/reset.html" style="padding:0.1rem 0.25rem;">
													Forgot Password?
												</a>
											</div>
										</div>
										<div class="row">
											<div class="col-12">
												<button type="submit" class="btn btn-warning btn-block">
													LOGIN
												</button>
											</div>
										</div>
									</form>
								</div>
							</div>
							<div class="card card-register text-white bg-primary py-5 d-md-down-none">
								<div class="card-body text-center">
									<div>
										<h2>Sign up</h2>
										<p>Create new Bet3Up account to get benefits: Faster BET on lotteries and sports, keep bet and profile informations, get first deposit bonus, referral bonus etc.</p>
										<a class="btn btn-lg btn-success mt-3" href="register.php">Register Now</a>
									</div>
								</div>
							</div>
						</div>
						
						<div class="card card-register text-white bg-primary mt-3 py-5 d-md-down-none">
							<div class="card-body text-center">
								<div class="social-items">
									<a class="nav-link link-whatsapp" href="https://api.whatsapp.com/send?phone=+919007326847" target="_blank">
										<img src="images/icon-whatsapp.png" width="32" alt="WhatsApp" />
										<span class="link-label">+91 90073 26847</span>
									</a>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<?php include('inc/footer.php') ?>
