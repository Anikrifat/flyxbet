<?php include('inc/header.php'); ?>

<div class="container">
	<div class="row">
		<div class="site-content col-md-12">
			<div class="row justify-content-center">
				<div class="col-md-12">
					<h1 class="page-title">Edit Profile Information</h1>
				</div>
			</div>
			<div class="card bg-white mb-3">
				<div class="card-header">Profile Information</div>
				<div class="card-body">
					<form id="frmUpdaeProfile" method="POST" action="#" novalidate="novalidate">
						<div class="form-group row">
							<div class="col-md-6">
								<div class="form-check">
									<label class="form-check-label" for="gender1">
										<input type="radio" name="gender" id="gender1" value="Male" autocomplete="off" checked=""> Male
									</label>
									<label class="form-check-label" for="gender2">
										<input type="radio" name="gender" id="gender2" value="Female" autocomplete="off"> Female
									</label>
									<label class="form-check-label" for="gender3">
										<input type="radio" name="gender" id="gender3" value="Others" autocomplete="off"> Others
									</label>
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="input-group">
									<input id="firstName" type="text" class="form-control valid" name="first_name" value="anik" placeholder="First Name" required="" autocomplete="first_name" autofocus="" aria-invalid="false">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<input id="lastName" type="text" class="form-control " name="last_name" value="rifat" placeholder="Last Name" required="" autocomplete="last_name">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="input-group">
									<input id="email" type="email" class="form-control " name="email" value="reafatul@gmail.com" placeholder="user@example.com" required="" autocomplete="email" readonly="">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<input id="profileDob" type="text" class="form-control " name="dob" value="10-02-2001" placeholder="Date of Birth" autocomplete="off" readonly="">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="input-group">
									<div class="dropdown bootstrap-select form-control custom-select"><select id="inputCountry" class="form-control custom-select selectpicker" name="country" tabindex="-98">
											<option data-icon="flag-icon-af" value="AF">Afghanistan</option>
											<option data-icon="flag-icon-al" value="AL">Albania</option>
											<option data-icon="flag-icon-aq" value="AQ">Antarctica</option>
											<option data-icon="flag-icon-dz" value="DZ">Algeria</option>
											<option data-icon="flag-icon-as" value="AS">American Samoa</option>
											<option data-icon="flag-icon-ad" value="AD">Andorra</option>
											<option data-icon="flag-icon-ao" value="AO">Angola</option>
											<option data-icon="flag-icon-ag" value="AG">Antigua and Barbuda</option>
											<option data-icon="flag-icon-az" value="AZ">Azerbaijan</option>
											<option data-icon="flag-icon-ar" value="AR">Argentina</option>
											<option data-icon="flag-icon-au" value="AU">Australia</option>
											<option data-icon="flag-icon-at" value="AT">Austria</option>
											<option data-icon="flag-icon-bs" value="BS">Bahamas</option>
											<option data-icon="flag-icon-bh" value="BH">Bahrain</option>
											<option data-icon="flag-icon-bd" value="BD" selected="selected">Bangladesh</option>
											<option data-icon="flag-icon-am" value="AM">Armenia</option>
											<option data-icon="flag-icon-bb" value="BB">Barbados</option>
											<option data-icon="flag-icon-be" value="BE">Belgium</option>
											<option data-icon="flag-icon-bm" value="BM">Bermuda</option>
											<option data-icon="flag-icon-bt" value="BT">Bhutan</option>
											<option data-icon="flag-icon-bo" value="BO">Bolivia, Plurinational State of</option>
											<option data-icon="flag-icon-ba" value="BA">Bosnia and Herzegovina</option>
											<option data-icon="flag-icon-bw" value="BW">Botswana</option>
											<option data-icon="flag-icon-bv" value="BV">Bouvet Island</option>
											<option data-icon="flag-icon-br" value="BR">Brazil</option>
											<option data-icon="flag-icon-bz" value="BZ">Belize</option>
											<option data-icon="flag-icon-io" value="IO">British Indian Ocean Territory</option>
											<option data-icon="flag-icon-sb" value="SB">Solomon Islands</option>
											<option data-icon="flag-icon-vg" value="VG">Virgin Islands, British</option>
											<option data-icon="flag-icon-bn" value="BN">Brunei Darussalam</option>
											<option data-icon="flag-icon-bg" value="BG">Bulgaria</option>
											<option data-icon="flag-icon-mm" value="MM">Myanmar</option>
											<option data-icon="flag-icon-bi" value="BI">Burundi</option>
											<option data-icon="flag-icon-by" value="BY">Belarus</option>
											<option data-icon="flag-icon-kh" value="KH">Cambodia</option>
											<option data-icon="flag-icon-cm" value="CM">Cameroon</option>
											<option data-icon="flag-icon-ca" value="CA">Canada</option>
											<option data-icon="flag-icon-cv" value="CV">Cape Verde</option>
											<option data-icon="flag-icon-ky" value="KY">Cayman Islands</option>
											<option data-icon="flag-icon-cf" value="CF">Central African Republic</option>
											<option data-icon="flag-icon-lk" value="LK">Sri Lanka</option>
											<option data-icon="flag-icon-td" value="TD">Chad</option>
											<option data-icon="flag-icon-cl" value="CL">Chile</option>
											<option data-icon="flag-icon-cn" value="CN">China</option>
											<option data-icon="flag-icon-tw" value="TW">Taiwan, Province of China</option>
											<option data-icon="flag-icon-cx" value="CX">Christmas Island</option>
											<option data-icon="flag-icon-cc" value="CC">Cocos (Keeling) Islands</option>
											<option data-icon="flag-icon-co" value="CO">Colombia</option>
											<option data-icon="flag-icon-km" value="KM">Comoros</option>
											<option data-icon="flag-icon-yt" value="YT">Mayotte</option>
											<option data-icon="flag-icon-cg" value="CG">Congo</option>
											<option data-icon="flag-icon-cd" value="CD">Congo, the Democratic Republic of the</option>
											<option data-icon="flag-icon-ck" value="CK">Cook Islands</option>
											<option data-icon="flag-icon-cr" value="CR">Costa Rica</option>
											<option data-icon="flag-icon-hr" value="HR">Croatia</option>
											<option data-icon="flag-icon-cu" value="CU">Cuba</option>
											<option data-icon="flag-icon-cy" value="CY">Cyprus</option>
											<option data-icon="flag-icon-cz" value="CZ">Czech Republic</option>
											<option data-icon="flag-icon-bj" value="BJ">Benin</option>
											<option data-icon="flag-icon-dk" value="DK">Denmark</option>
											<option data-icon="flag-icon-dm" value="DM">Dominica</option>
											<option data-icon="flag-icon-do" value="DO">Dominican Republic</option>
											<option data-icon="flag-icon-ec" value="EC">Ecuador</option>
											<option data-icon="flag-icon-sv" value="SV">El Salvador</option>
											<option data-icon="flag-icon-gq" value="GQ">Equatorial Guinea</option>
											<option data-icon="flag-icon-et" value="ET">Ethiopia</option>
											<option data-icon="flag-icon-er" value="ER">Eritrea</option>
											<option data-icon="flag-icon-ee" value="EE">Estonia</option>
											<option data-icon="flag-icon-fo" value="FO">Faroe Islands</option>
											<option data-icon="flag-icon-fk" value="FK">Falkland Islands (Malvinas)</option>
											<option data-icon="flag-icon-gs" value="GS">South Georgia and the South Sandwich Islands</option>
											<option data-icon="flag-icon-fj" value="FJ">Fiji</option>
											<option data-icon="flag-icon-fi" value="FI">Finland</option>
											<option data-icon="flag-icon-ax" value="AX">Åland Islands</option>
											<option data-icon="flag-icon-fr" value="FR">France</option>
											<option data-icon="flag-icon-gf" value="GF">French Guiana</option>
											<option data-icon="flag-icon-pf" value="PF">French Polynesia</option>
											<option data-icon="flag-icon-tf" value="TF">French Southern Territories</option>
											<option data-icon="flag-icon-dj" value="DJ">Djibouti</option>
											<option data-icon="flag-icon-ga" value="GA">Gabon</option>
											<option data-icon="flag-icon-ge" value="GE">Georgia</option>
											<option data-icon="flag-icon-gm" value="GM">Gambia</option>
											<option data-icon="flag-icon-ps" value="PS">Palestinian Territory, Occupied</option>
											<option data-icon="flag-icon-de" value="DE">Germany</option>
											<option data-icon="flag-icon-gh" value="GH">Ghana</option>
											<option data-icon="flag-icon-gi" value="GI">Gibraltar</option>
											<option data-icon="flag-icon-ki" value="KI">Kiribati</option>
											<option data-icon="flag-icon-gr" value="GR">Greece</option>
											<option data-icon="flag-icon-gl" value="GL">Greenland</option>
											<option data-icon="flag-icon-gd" value="GD">Grenada</option>
											<option data-icon="flag-icon-gp" value="GP">Guadeloupe</option>
											<option data-icon="flag-icon-gu" value="GU">Guam</option>
											<option data-icon="flag-icon-gt" value="GT">Guatemala</option>
											<option data-icon="flag-icon-gn" value="GN">Guinea</option>
											<option data-icon="flag-icon-gy" value="GY">Guyana</option>
											<option data-icon="flag-icon-ht" value="HT">Haiti</option>
											<option data-icon="flag-icon-hm" value="HM">Heard Island and McDonald Islands</option>
											<option data-icon="flag-icon-va" value="VA">Holy See (Vatican City State)</option>
											<option data-icon="flag-icon-hn" value="HN">Honduras</option>
											<option data-icon="flag-icon-hk" value="HK">Hong Kong</option>
											<option data-icon="flag-icon-hu" value="HU">Hungary</option>
											<option data-icon="flag-icon-is" value="IS">Iceland</option>
											<option data-icon="flag-icon-in" value="IN">India</option>
											<option data-icon="flag-icon-id" value="ID">Indonesia</option>
											<option data-icon="flag-icon-ir" value="IR">Iran, Islamic Republic of</option>
											<option data-icon="flag-icon-iq" value="IQ">Iraq</option>
											<option data-icon="flag-icon-ie" value="IE">Ireland</option>
											<option data-icon="flag-icon-il" value="IL">Israel</option>
											<option data-icon="flag-icon-it" value="IT">Italy</option>
											<option data-icon="flag-icon-ci" value="CI">Côte d'Ivoire</option>
											<option data-icon="flag-icon-jm" value="JM">Jamaica</option>
											<option data-icon="flag-icon-jp" value="JP">Japan</option>
											<option data-icon="flag-icon-kz" value="KZ">Kazakhstan</option>
											<option data-icon="flag-icon-jo" value="JO">Jordan</option>
											<option data-icon="flag-icon-ke" value="KE">Kenya</option>
											<option data-icon="flag-icon-kp" value="KP">Korea, Democratic People's Republic of</option>
											<option data-icon="flag-icon-kr" value="KR">Korea, Republic of</option>
											<option data-icon="flag-icon-kw" value="KW">Kuwait</option>
											<option data-icon="flag-icon-kg" value="KG">Kyrgyzstan</option>
											<option data-icon="flag-icon-la" value="LA">Lao People's Democratic Republic</option>
											<option data-icon="flag-icon-lb" value="LB">Lebanon</option>
											<option data-icon="flag-icon-ls" value="LS">Lesotho</option>
											<option data-icon="flag-icon-lv" value="LV">Latvia</option>
											<option data-icon="flag-icon-lr" value="LR">Liberia</option>
											<option data-icon="flag-icon-ly" value="LY">Libya</option>
											<option data-icon="flag-icon-li" value="LI">Liechtenstein</option>
											<option data-icon="flag-icon-lt" value="LT">Lithuania</option>
											<option data-icon="flag-icon-lu" value="LU">Luxembourg</option>
											<option data-icon="flag-icon-mo" value="MO">Macao</option>
											<option data-icon="flag-icon-mg" value="MG">Madagascar</option>
											<option data-icon="flag-icon-mw" value="MW">Malawi</option>
											<option data-icon="flag-icon-my" value="MY">Malaysia</option>
											<option data-icon="flag-icon-mv" value="MV">Maldives</option>
											<option data-icon="flag-icon-ml" value="ML">Mali</option>
											<option data-icon="flag-icon-mt" value="MT">Malta</option>
											<option data-icon="flag-icon-mq" value="MQ">Martinique</option>
											<option data-icon="flag-icon-mr" value="MR">Mauritania</option>
											<option data-icon="flag-icon-mu" value="MU">Mauritius</option>
											<option data-icon="flag-icon-mx" value="MX">Mexico</option>
											<option data-icon="flag-icon-mc" value="MC">Monaco</option>
											<option data-icon="flag-icon-mn" value="MN">Mongolia</option>
											<option data-icon="flag-icon-md" value="MD">Moldova, Republic of</option>
											<option data-icon="flag-icon-me" value="ME">Montenegro</option>
											<option data-icon="flag-icon-ms" value="MS">Montserrat</option>
											<option data-icon="flag-icon-ma" value="MA">Morocco</option>
											<option data-icon="flag-icon-mz" value="MZ">Mozambique</option>
											<option data-icon="flag-icon-om" value="OM">Oman</option>
											<option data-icon="flag-icon-na" value="NA">Namibia</option>
											<option data-icon="flag-icon-nr" value="NR">Nauru</option>
											<option data-icon="flag-icon-np" value="NP">Nepal</option>
											<option data-icon="flag-icon-nl" value="NL">Netherlands</option>
											<option data-icon="flag-icon-cw" value="CW">Curaçao</option>
											<option data-icon="flag-icon-aw" value="AW">Aruba</option>
											<option data-icon="flag-icon-sx" value="SX">Sint Maarten (Dutch part)</option>
											<option data-icon="flag-icon-bq" value="BQ">Bonaire, Sint Eustatius and Saba</option>
											<option data-icon="flag-icon-nc" value="NC">New Caledonia</option>
											<option data-icon="flag-icon-vu" value="VU">Vanuatu</option>
											<option data-icon="flag-icon-nz" value="NZ">New Zealand</option>
											<option data-icon="flag-icon-ni" value="NI">Nicaragua</option>
											<option data-icon="flag-icon-ne" value="NE">Niger</option>
											<option data-icon="flag-icon-ng" value="NG">Nigeria</option>
											<option data-icon="flag-icon-nu" value="NU">Niue</option>
											<option data-icon="flag-icon-nf" value="NF">Norfolk Island</option>
											<option data-icon="flag-icon-no" value="NO">Norway</option>
											<option data-icon="flag-icon-mp" value="MP">Northern Mariana Islands</option>
											<option data-icon="flag-icon-um" value="UM">United States Minor Outlying Islands</option>
											<option data-icon="flag-icon-fm" value="FM">Micronesia, Federated States of</option>
											<option data-icon="flag-icon-mh" value="MH">Marshall Islands</option>
											<option data-icon="flag-icon-pw" value="PW">Palau</option>
											<option data-icon="flag-icon-pk" value="PK">Pakistan</option>
											<option data-icon="flag-icon-pa" value="PA">Panama</option>
											<option data-icon="flag-icon-pg" value="PG">Papua New Guinea</option>
											<option data-icon="flag-icon-py" value="PY">Paraguay</option>
											<option data-icon="flag-icon-pe" value="PE">Peru</option>
											<option data-icon="flag-icon-ph" value="PH">Philippines</option>
											<option data-icon="flag-icon-pn" value="PN">Pitcairn</option>
											<option data-icon="flag-icon-pl" value="PL">Poland</option>
											<option data-icon="flag-icon-pt" value="PT">Portugal</option>
											<option data-icon="flag-icon-gw" value="GW">Guinea-Bissau</option>
											<option data-icon="flag-icon-tl" value="TL">Timor-Leste</option>
											<option data-icon="flag-icon-pr" value="PR">Puerto Rico</option>
											<option data-icon="flag-icon-qa" value="QA">Qatar</option>
											<option data-icon="flag-icon-re" value="RE">Réunion</option>
											<option data-icon="flag-icon-ro" value="RO">Romania</option>
											<option data-icon="flag-icon-ru" value="RU">Russian Federation</option>
											<option data-icon="flag-icon-rw" value="RW">Rwanda</option>
											<option data-icon="flag-icon-bl" value="BL">Saint Barthélemy</option>
											<option data-icon="flag-icon-sh" value="SH">Saint Helena, Ascension and Tristan da Cunha</option>
											<option data-icon="flag-icon-kn" value="KN">Saint Kitts and Nevis</option>
											<option data-icon="flag-icon-ai" value="AI">Anguilla</option>
											<option data-icon="flag-icon-lc" value="LC">Saint Lucia</option>
											<option data-icon="flag-icon-mf" value="MF">Saint Martin (French part)</option>
											<option data-icon="flag-icon-pm" value="PM">Saint Pierre and Miquelon</option>
											<option data-icon="flag-icon-vc" value="VC">Saint Vincent and the Grenadines</option>
											<option data-icon="flag-icon-sm" value="SM">San Marino</option>
											<option data-icon="flag-icon-st" value="ST">Sao Tome and Principe</option>
											<option data-icon="flag-icon-sa" value="SA">Saudi Arabia</option>
											<option data-icon="flag-icon-sn" value="SN">Senegal</option>
											<option data-icon="flag-icon-rs" value="RS">Serbia</option>
											<option data-icon="flag-icon-sc" value="SC">Seychelles</option>
											<option data-icon="flag-icon-sl" value="SL">Sierra Leone</option>
											<option data-icon="flag-icon-sg" value="SG">Singapore</option>
											<option data-icon="flag-icon-sk" value="SK">Slovakia</option>
											<option data-icon="flag-icon-vn" value="VN">Viet Nam</option>
											<option data-icon="flag-icon-si" value="SI">Slovenia</option>
											<option data-icon="flag-icon-so" value="SO">Somalia</option>
											<option data-icon="flag-icon-za" value="ZA">South Africa</option>
											<option data-icon="flag-icon-zw" value="ZW">Zimbabwe</option>
											<option data-icon="flag-icon-es" value="ES">Spain</option>
											<option data-icon="flag-icon-ss" value="SS">South Sudan</option>
											<option data-icon="flag-icon-sd" value="SD">Sudan</option>
											<option data-icon="flag-icon-eh" value="EH">Western Sahara</option>
											<option data-icon="flag-icon-sr" value="SR">Suriname</option>
											<option data-icon="flag-icon-sj" value="SJ">Svalbard and Jan Mayen</option>
											<option data-icon="flag-icon-sz" value="SZ">Swaziland</option>
											<option data-icon="flag-icon-se" value="SE">Sweden</option>
											<option data-icon="flag-icon-ch" value="CH">Switzerland</option>
											<option data-icon="flag-icon-sy" value="SY">Syrian Arab Republic</option>
											<option data-icon="flag-icon-tj" value="TJ">Tajikistan</option>
											<option data-icon="flag-icon-th" value="TH">Thailand</option>
											<option data-icon="flag-icon-tg" value="TG">Togo</option>
											<option data-icon="flag-icon-tk" value="TK">Tokelau</option>
											<option data-icon="flag-icon-to" value="TO">Tonga</option>
											<option data-icon="flag-icon-tt" value="TT">Trinidad and Tobago</option>
											<option data-icon="flag-icon-ae" value="AE">United Arab Emirates</option>
											<option data-icon="flag-icon-tn" value="TN">Tunisia</option>
											<option data-icon="flag-icon-tr" value="TR">Turkey</option>
											<option data-icon="flag-icon-tm" value="TM">Turkmenistan</option>
											<option data-icon="flag-icon-tc" value="TC">Turks and Caicos Islands</option>
											<option data-icon="flag-icon-tv" value="TV">Tuvalu</option>
											<option data-icon="flag-icon-ug" value="UG">Uganda</option>
											<option data-icon="flag-icon-ua" value="UA">Ukraine</option>
											<option data-icon="flag-icon-mk" value="MK">Macedonia, the former Yugoslav Republic of</option>
											<option data-icon="flag-icon-eg" value="EG">Egypt</option>
											<option data-icon="flag-icon-gb" value="GB">United Kingdom</option>
											<option data-icon="flag-icon-gg" value="GG">Guernsey</option>
											<option data-icon="flag-icon-je" value="JE">Jersey</option>
											<option data-icon="flag-icon-im" value="IM">Isle of Man</option>
											<option data-icon="flag-icon-tz" value="TZ">Tanzania, United Republic of</option>
											<option data-icon="flag-icon-us" value="US">United States</option>
											<option data-icon="flag-icon-vi" value="VI">Virgin Islands, U.S.</option>
											<option data-icon="flag-icon-bf" value="BF">Burkina Faso</option>
											<option data-icon="flag-icon-uy" value="UY">Uruguay</option>
											<option data-icon="flag-icon-uz" value="UZ">Uzbekistan</option>
											<option data-icon="flag-icon-ve" value="VE">Venezuela, Bolivarian Republic of</option>
											<option data-icon="flag-icon-wf" value="WF">Wallis and Futuna</option>
											<option data-icon="flag-icon-ws" value="WS">Samoa</option>
											<option data-icon="flag-icon-ye" value="YE">Yemen</option>
											<option data-icon="flag-icon-zm" value="ZM">Zambia</option>
										</select><button type="button" class="btn dropdown-toggle btn-light" data-toggle="dropdown" role="combobox" aria-owns="bs-select-1" aria-haspopup="listbox" aria-expanded="false" data-id="inputCountry" title="Bangladesh">
											<div class="filter-option">
												<div class="filter-option-inner">
													<div class="filter-option-inner-inner"><i class="flag-icon flag-icon-bd"></i>&nbsp;Bangladesh</div>
												</div>
											</div>
										</button>
										<div class="dropdown-menu ">
											<div class="bs-searchbox"><input type="search" class="form-control" autocomplete="off" role="combobox" aria-label="Search" aria-controls="bs-select-1" aria-autocomplete="list"></div>
											<div class="inner show" role="listbox" id="bs-select-1" tabindex="-1">
												<ul class="dropdown-menu inner show" role="presentation"></ul>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div class="col-md-6">
							<input id="phone" name="phone" type="tel">
							</div>
							<script>
								 var input = document.querySelector("#phone");
    window.intlTelInput(input, {
      // allowDropdown: false,
      // autoHideDialCode: false,
      // autoPlaceholder: "off",
      // dropdownContainer: document.body,
      // excludeCountries: ["us"],
      // formatOnDisplay: false,
      // geoIpLookup: function(callback) {
      //   $.get("http://ipinfo.io", function() {}, "jsonp").always(function(resp) {
      //     var countryCode = (resp && resp.country) ? resp.country : "";
      //     callback(countryCode);
      //   });
      // },
      // hiddenInput: "full_number",
      // initialCountry: "auto",
      // localizedCountries: { 'de': 'Deutschland' },
      // nationalMode: false,
      // onlyCountries: ['us', 'gb', 'ch', 'ca', 'do'],
      // placeholderNumberType: "MOBILE",
      // preferredCountries: ['cn', 'jp'],
      // separateDialCode: true,
     // utilsScript: "build/js/utils.js",
    });
							</script>
						</div>
						<div class="form-group row">
							<div class="col-md-6">
								<div class="input-group">
									<input id="city" type="text" class="form-control " name="city" value="chittagong" placeholder="City" autocomplete="city">
								</div>
							</div>
							<div class="col-md-6">
								<div class="input-group">
									<input id="nationality" type="text" class="form-control " name="nationality" value="" placeholder="Nationality" autocomplete="nationality">
								</div>
							</div>
						</div>
						<div class="form-group row">
							<div class="col-md-12">
								<div class="input-group">
									<textarea class="form-control " id="about" name="about" rows="5" placeholder="About Me"></textarea>
								</div>
							</div>
						</div>
						<div class="form-group row mb-0">
							<div class="col-md-6 offset-md-3">
								<button type="submit" class="btn btn-success btn-block">
									Update Profile
								</button>
							</div>
						</div>
					</form>
				</div>
				<div class="card-footer">&nbsp;</div>
			</div>

		</div>
	</div>
</div>
<?php include('inc/footer.php'); ?>