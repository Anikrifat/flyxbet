</main>
</div>
<footer id="siteFooter" class="site-footer text-white bg-primary py-4">
	<div class="bbbb">
		<div class="container">
			<div class="d-flex justify-content-around">
				<div class="social fot-span">
					<span><a href="#" class="fot-ic"><i class="fab fa-facebook-f"></i></a></span>
					<span><a href="#" class="fot-ic"><i class="fab fa-instagram"></i></a></span>
					<span><a href="#" class="fot-ic"><i class="fab fa-twitter"></i></a></span>
				</div>
				<div class="download fot-span">
					<span><a href="#" class="fot-ic"><i class="fab fa-google-play"></i></a></span>
					<span><a href="#" class="fot-ic"><i class="fab fa-apple"></i></a></span>
				</div>
				<div class="other fot-span">
					<span><a href="#" class="fot-ic font-weight-bold">18+</a></span>

				</div>
			</div>
		</div>
		<div class="payment-pic mt-5 p-2">
			<div class="container">
				<div class="d-flex justify-content-around">
					<img src="assets/img/payment-methods/mastercard.png" alt="" class="img-payment">
					<img src="assets/img/payment-methods/neteller.png" alt="" class="img-payment">
					<img src="assets/img/payment-methods/skrill.png" alt="" class="img-payment">
					<img src="assets/img/payment-methods/visacard.png" alt="" class="img-payment">
					<img src="assets/img/payment-methods/p1.png" alt="" class="img-payment">
					<img src="assets/img/payment-methods/p2.png" alt="" class="img-payment">
					<img src="assets/img/payment-methods/p3.png" alt="" class="img-payment">
				</div>
			</div>
		</div>
	</div>
	</div>
</footer>
<style>

</style>


<script src="assets/js/jquery-3.5.1.min.js"></script>
<script src="assets/js/jquery-migrate-1.2.1.min.js"></script>
<script src="assets/js/jquery.cookie.min.js"></script>
<script src="assets/js/jquery.countdown.min.js"></script>
<script src="assets/js/isotope.pkgd.min.js"></script>
<script src="assets/js/bootstrap.bundle.min.js"></script>
<script src="assets/js/moment-with-locales.js"></script>
<script src="assets/js/jquery.datetimepicker.full.min.js"></script>
<script src="assets/js/bootstrap-select.min.js"></script>
<script src="assets/js/jquery.fancybox.min.js"></script>
<script src="assets/js/intlTelInput.min.js"></script>
<script src="assets/js/jquery.validate.js"></script>
<script src="assets/js/jquery.dataTables.min.js"></script>
<script src="assets/js/dataTables.bootstrap4.min.js"></script>
<script src="assets/js/slick.min.js"></script>
<script src="assets/js/mobile-detect.js"></script>
<script src="../unpkg.com/sweetalert%402.1.2/dist/sweetalert.min.js"></script>
<script src="assets/js/ma5-menu.js" media="screen and (max-width: 991px)"></script>

<script src="https://www.youtube.com/iframe_api"></script>
<script src="../www.google.com/recaptcha/apibd7d.js?render=6LcCNA8aAAAAAPucBgiU7e-Bmh5lR_1U6qRlHRd8"></script>
<script src="assets/js/app.js" defer></script>

<script async src="https://www.googletagmanager.com/gtag/js?id=G-9S7VJHRP9S"></script>
<script>
	window.dataLayer = window.dataLayer || [];

	function gtag() {
		dataLayer.push(arguments);
	}
	gtag('js', new Date());

	gtag('config', 'G-9S7VJHRP9S');

	jQuery(document).ready(function($) {
		$("#frmPopoverLogin").validate({
			rules: {
				username: {
					required: true
				},
				password: {
					required: true,
					minlength: 6
				}
			},
			messages: {
				username: "Please enter user login!",
				password: {
					required: "Please enter login password!",
					minlength: "Enter password at least 6 characters!"
				}
			}
		});
	});

	jQuery(document).ready(function($) {
		$('.home-banner-slider').slick({
			slidesToShow: 1,
			slidesToScroll: 1,
			autoplay: true,
			autoplaySpeed: 4000,
			dots: false,
			arrows: true,
			infinite: true,
			speed: 500,
			fade: true,
			cssEase: 'linear',
			responsive: [{
				breakpoint: 639,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1,
					arrows: false,
					dots: true
				}
			}]
		});
	});
	$('.home-banner-slider').on("beforeChange", function() {
		$('.caption>h2').removeClass('animate__animated animate__backInDown').hide();
		$('.caption>div').removeClass('animate__animated animate__fadeInUp').hide();
		$('.caption>.btn').removeClass('animate__animated animate__fadeInUp').hide();
		setTimeout(() => {
			$('.caption>h2').addClass('animate__animated animate__backInDown').show();
			$('.caption>div').addClass('animate__animated animate__fadeInUp').show();
			$('.caption>.btn').addClass('animate__animated animate__fadeInUp').show();
		}, 600);
	});
	jQuery(document).ready(function($) {
		var customClass = 'guest-user';

		ma5menu({
			menu: '.navbar-ma5menu',
			activeClass: 'active',
			customClass: customClass,
			header: '#ma5menu-header',
			footer: '#ma5menu-footer',
			position: 'right',
			closeOnBodyClick: true
		});

		$(document).on('click', '.ma5menu-notification', function(event) {
			event.preventDefault();
		});
	});
</script>

</body>


</html>