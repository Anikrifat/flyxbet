<!doctype html>
<html lang="en" class="incoming">

<!-- Mirrored from www.bet3up.com/ by HTTrack Website Copier/3.x [XR&CO'2014], Mon, 12 Jul 2021 14:04:21 GMT -->
<!-- Added by HTTrack -->
<meta http-equiv="content-type" content="text/html;charset=UTF-8" /><!-- /Added by HTTrack -->

<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge" />
	<meta name="viewport" content="width=device-width,height=device-height,initial-scale=1,maximum-scale=1,user-scalable=no,viewport-fit=cover">
	<title>FLyxbet</title>
	<link rel="dns-prefetch" href="http://fonts.gstatic.com/">
	<link href="https://fonts.googleapis.com/css2?family=Nunito:ital,wght@0,200;0,300;0,400;0,600;0,700;0,800;0,900;1,200;1,300;1,400;1,600;1,700;1,800;1,900&amp;display=swap" rel="stylesheet">
	<link href="https://fonts.googleapis.com/css2?family=Roboto:ital,wght@0,300;0,400;0,500;0,700;0,900;1,300;1,400;1,500&amp;display=swap" rel="stylesheet">

	<link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.15.3/css/all.css" integrity="sha512-3icgkoIO5qm2D4bGSUkPqeQ96LS8+ukJC7Eqhl1H5B2OJMEnFqLmNDxXVmtV/eq5M65tTDkUYS/Q0P4gvZv+yA==" crossorigin="anonymous" referrerpolicy="no-referrer" />
	<link rel="stylesheet" href="assets/css/bootstrap.min.css" media="all" />
	<link rel="stylesheet" href="assets/css/bootstrap-select.min.css" media="all" />
	<link rel="stylesheet" href="assets/css/animate.min.css" media="all" />
	<link rel="stylesheet" href="assets/css/flag-icon.min.css" media="all" />
	<link rel="stylesheet" href="assets/css/slick.css" media="all" />
	<link rel="stylesheet" href="assets/css/slick-theme.css" media="all" />
	<link rel="stylesheet" href="assets/css/ma5-menu.css" media="all" />
	<link rel="stylesheet" href="assets/css/app.css" media="all" />
	<link rel="stylesheet" href="assets/css/style.css">

</head>

<body class="page-landing">
	<div id="pageLoader" class="page-loading">
		<div class="loader">Loading...</div>
	</div>
	<div id="appBet3Up" class="wrapper">
		<header id="siteHeader" class="site-header">
			<div class="header-top bg-primary">
				<div class="container">
					<nav class="navbar navbar-expand-lg navbar-dark">
						<a href="index.php" class="navbar-brand"><img src="assets/img/bet3up.svg" alt="Bet3Up" /></a>
						<div class="collapse show navbar-collapse navbar-common-navbar ml-auto" id="navbarCommonNavbar">
							<ul class="navbar-nav ml-auto">
								<li class="nav-item animated-nav-item">
									<a class="nav-link" href="register.php">
										<span class="material-icons">person_add</span>
										<!-- <span class="link-label">Register</span> -->
									</a>
								</li>
								<li class="nav-item animated-nav-item">
									<a class="nav-link" href="login.php"><span class="material-icons">login</span></a>

								</li>
								<li class="nav-item nav-item-menu">
									<button id="btnToggleHeaderNavbar" class="navbar-toggler ma5menu__toggle menu-menu" type="button" aria-label="Toggle Navigation">
										<span class="material-icons menu">sort</span>
									</button>
								</li>
							</ul>
						</div>
					</nav>

					<ul id="navbar-ma5menu" class="navbar-ma5menu">
						<li class="nav-item ">
							<a class="nav-link" href="register.php"><span class="material-icons">person_add</span>
								<span class="link-label">Register</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="login.php"><span class="material-icons">login</span><span class="link-label">Login</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="deposit.php"><span class="material-icons">library_add</span><span class="link-label">Deposit</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="withdrawal.php"><span class="material-icons">payments</span> <span class="link-label">Withdrawal</span></a>
						</li>

						<li class="nav-item ">
							<a class="nav-link" href="games.php"><span class="material-icons">payments</span> <span class="link-label">Livve Casico</span></a>
						</li>
						
						<li class="nav-item ">
							<a class="nav-link" href="profile.php"><span class="material-icons">account_balance</span><span class="link-label">Profile
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">result
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">bingo
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">toto
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">other games
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">casico
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">cricket
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">demo
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">demo
								</span></a>
						</li>
						<li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">sport
								</span></a>
						</li><li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">live
								</span></a>
						</li><li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">bet slip
								</span></a>
						</li><li class="nav-item ">
							<a class="nav-link" href="#"><span class="material-icons">account_balance</span><span class="link-label">RB games
								</span></a>
						</li>
						
					</ul>
				</div>
			</div>
			<div id="navbarToggleHeader" class="collapse navbar-collapse header-navbar bg-primary">
				<div class="close-navbar-icon bg-primary">
					<a href="index#" class="navbar-brand"><img src="assets/img/bet3up.svg" alt="" /></a>
					<button type="button" data-navbar="#navbarToggleHeader" class="close close-navbar" data-dismiss="alert"><span class="material-icons">cancel</span></button>
				</div>

				<nav class="navbar navbar-expand-lg navbar-dark">
					<ul class="navbar-nav navbar-toggle-header navbar-dark">
						<li class="nav-item">
							<a class="nav-link" href="index.php"><span class="material-icons">home</span><span class="link-label">Home</span></a>
						</li>


						<li class="nav-item">
							<a class="nav-link" href="deposit.php"><span class="material-icons">library_add</span><span class="link-label">Deposit</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="games.php"><span class="material-icons">library_add</span><span class="link-label">Play Games</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="withdrawal.php"><span class="material-icons">library_add</span><span class="link-label">Withdraw</span></a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="profile.php"><span class="material-icons">library_add</span><span class="link-label">profile</span></a>
						</li>


					</ul>
				</nav>
			</div>
	</div>



	</header>
	<main id="siteMain" class="site-main">